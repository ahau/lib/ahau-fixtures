# ahau-fixtures

A generator of mock ahau databases

## API

### `fixtures.createWhakapapaTree(ssb, groupId, N, cb)`

This script is designed to make a large whakapapa tree, it builds:
    - a whakapapaView
    - a tree with N profiles

- `ssb` : scuttlebutt instanace with `ssb-whakapapa`, `ssb-profile`, `ssb-tribes` installed
- `groupId` : group to publish the records to
- `N` : how many profiles should be in the whakapapa tree
- `cb` : a callback for when done

_If `cb` is ommitted, returns a Promise_

**Algorithm**
- setup
    1. create one profile, increment profileCount
    2. make it the focus of a whakapapaView
    3. push the profileId into a "queue"

- while profileCount < N
    1. pull a profileId from "queue"
    2. create some random number of children (profile + link to parent)
        - NOTE: if there are no more profiles in queue, and profileCount < N, we must create at least 1 child.
        - create profile, increment profileCounter
        - create childLink to parent
    3. push the childrens profileIds into the queue


This graph does not have partners, nor importantLinks

