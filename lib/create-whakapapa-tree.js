const pull = require('pull-stream')
const Pushable = require('pull-pushable')
const paraMap = require('pull-paramap')
const flatMap = require('pull-flatmap')
const { promisify } = require('util')

module.exports = function createWhakapapaTree (ssb, groupId, N, cb) {
  if (cb === undefined) return promisify(createWhakapapaTree)(ssb, groupId, N)

  let profileCount = 0
  const queue = Pushable()
  const authors = { add: ['*'] }

  // start
  console.time('create-whakapapa-tree')
  // make initial profile
  createPerson((err, profileId) => {
    if (err) return cb(err)

    queue.push(profileId)

    // create a view which points to that profile (focus)
    ssb.whakapapa.view.create(
      {
        name: 'bulk-' + N,
        focus: profileId,
        authors,
        recps: [groupId]
      },
      (err, viewId) => {
        if (err) return cb(err)

        // build a whakapapa below that profile
        createTree((err) => err ? cb(err) : cb(null, viewId))
      }
    )
  })

  //

  function createPerson (cb) {
    ssb.profile.person.group.create(
      {
        preferredName: String(++profileCount),
        authors,
        recps: [groupId]
      },
      cb
    )
  }
  function createChild (parentId, cb) {
    if (profileCount >= N) return cb(null)

    createPerson((err, childId) => {
      if (err) return cb(err)
      process.stdout.write('.')

      queue.push(childId)

      ssb.whakapapa.child.create(
        {
          parent: parentId,
          child: childId
        },
        {
          relationshipType: 'birth',
          recps: [groupId]
        },
        cb
      )
    })
  }

  function createTree (cb) {
    pull(
      queue,
      flatMap(parentId => {
        let numberOfChildren = Math.floor(Math.random() * 7)
        if (queue.buffer.length === 0 && numberOfChildren === 0) numberOfChildren++

        return new Array(numberOfChildren).fill(parentId)
      }),
      paraMap(createChild, 3),

      pull.take(N - 1),
      pull.collect((err) => {
        if (err) return cb(err)

        console.timeEnd('create-whakapapa-tree')
        console.log('profiles:', profileCount)

        cb(null)
      })
    )
  }
}
